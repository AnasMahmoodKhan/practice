import React, { Component } from "react";
import { connect } from "react-redux";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import * as contactAction from "./actions/contactAction";

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFormClick = this.handleFormClick.bind(this);

    this.state = {
      name: "",
      formOpen: false,
      formIndexOpen : -1,
      formViewOpen : false
    };
  }

  handleChange(e) {
    this.setState({
      name: e.target.value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      formOpen: !this.state.formOpen,
    });
  }
  
  handleFormClick(index){
    this.setState({
      formIndexOpen : index,
      formViewOpen : !this.state.formViewOpen
    })
  }

  listView(data, index) {
    return (
      <div key={index} className="row p-1" onClick={()=>this.handleFormClick(index)}>
        <div className="col-md-10">
          <li  className="list-group-item clearfix">
            {data.name}
          </li>
        </div>
        <div className="col-md-2 p-1">
          <button
            onClick={(e) => this.deleteContact(e, index)}
            className="btn btn-danger"
          >
            Remove
          </button>
        </div>
          {this.state.formIndexOpen === index && this.state.formViewOpen && (
            <React.Fragment>
              <div className="col-md-12 m-1">
                <ul>
                  <li>{data.firstname}</li>
                  <li>{data.lastname}</li>
                  <li>{data.email}</li>
                  <li>{data.contact}</li>
                </ul>
              </div>
            </React.Fragment>
          )}
     
      </div>
    );
  }

  deleteContact(e, index) {
    e.preventDefault();
    this.props.deleteContact(index);
  }

  contactForm() {
    return (
      <React.Fragment>
        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            email: "",
            contact: "",
          }}
          validationSchema={Yup.object().shape({
            firstName: Yup.string().required("First Name is required"),
            lastName: Yup.string().required("Last Name is required"),
            email: Yup.string()
              .email("Email is invalid")
              .required("Email is required"),
            contact: Yup.number().required("Email is required"),
          })}
          onSubmit={(fields) => {
            let values = {
              name: this.state.name,
              firstname: fields.firstName,
              lastname: fields.lastName,
              email: fields.email,
              contact: fields.contact,
            };
            this.props.createContact(values);
            this.setState({
              name: "",
              formOpen: !this.state.formOpen,
            });
          }}
          render={({ errors, status, touched }) => (
            <Form>
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <Field
                  name="firstName"
                  type="text"
                  className={
                    "form-control" +
                    (errors.firstName && touched.firstName ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="firstName"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="lastName">Last Name</label>
                <Field
                  name="lastName"
                  type="text"
                  className={
                    "form-control" +
                    (errors.lastName && touched.lastName ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="lastName"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <Field
                  name="email"
                  type="text"
                  className={
                    "form-control" +
                    (errors.email && touched.email ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="email"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <label htmlFor="contact">Phone</label>
                <Field
                  name="contact"
                  type="text"
                  className={
                    "form-control" +
                    (errors.contact && touched.contact ? " is-invalid" : "")
                  }
                />
                <ErrorMessage
                  name="contact"
                  component="div"
                  className="invalid-feedback"
                />
              </div>
              <div className="form-group">
                <button type="submit" className="btn btn-primary mr-2">
                  Submit
                </button>
                <button type="reset" className="btn btn-secondary">
                  Reset
                </button>
              </div>
            </Form>
          )}
        />
      </React.Fragment>
    );
  }

  render() {
    return (
      <div className="container">
        <h1>Clientside Contacts Application</h1>
        <hr />
        <div>
          <h3>Add Contact Form</h3>
          <form onSubmit={this.handleSubmit}>
            <div className="row">
              <input
                type="text"
                onChange={this.handleChange}
                className="form-control col-10"
                value={this.state.name}
              />
              <div className="col-2">
                <input type="submit" className="btn btn-success" value="ADD" />
              </div>
            </div>

            <br />
          </form>
          <hr />
          {this.state.formOpen && this.contactForm()}
          <hr />
          {
            <ul className="list-group">
              {this.props.contacts.map((contact, i) =>
                this.listView(contact, i)
              )}
            </ul>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    contacts: state.contacts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createContact: (contact) => dispatch(contactAction.createContact(contact)),
    deleteContact: (index) => dispatch(contactAction.deleteContact(index)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
